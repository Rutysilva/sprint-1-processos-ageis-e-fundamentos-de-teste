# Atividade - Priorização
### Escolher segmento e aplicação na internet, e criar lista de fluxos prioritários para validação
[Voltar a página inicial](https://gitlab.com/Rutysilva/sprint-1-processos-ageis-e-fundamentos-de-teste/-/blob/main/README.md)

<b>- Aplicação</b>

Mercado Live : uma empresa varejista, completamente digital, que permite que qualquer pessoa anuncie, compre e venda produtos dentro de sua plataforma.

<b>- Segmento</b>

E-Commerce.

<b>- Objetivo principal</b>

Facilitar a compra e venda.

<b>- Lista de fluxos</b>

1. Registro de usuário: Validar o fluxo de registro de novos usuários, garantindo que os dados sejam capturados corretamente, incluindo verificação de e-mail, senha segura e conformidade com políticas de privacidade.

2. Login de usuário: Validar o processo de login, garantindo que os usuários possam autenticar-se com segurança utilizando suas credenciais corretas, como e-mail ou nome de usuário e senha. Isso inclui a verificação de autenticação adequada, tratamento de erros de login incorreto e proteção contra tentativas de acesso não autorizadas.

3. Pesquisa de produtos: Verificar se a pesquisa de produtos retorna resultados relevantes e precisos com base nos critérios de busca dos usuários, levando em consideração a velocidade e a eficiência das consultas.

4. Visualização de detalhes do produto: Validar se os detalhes do produto são exibidos corretamente, incluindo descrição, preço, fotos, informações sobre o vendedor, condições de envio e disponibilidade.

5. Adição ao carrinho de compras: Verificar se os produtos são adicionados corretamente ao carrinho de compras, levando em conta a quantidade, variantes (se aplicável) e a atualização adequada do total do carrinho.

6. Processo de checkout: Validar o fluxo de pagamento e conclusão da compra, garantindo que o usuário possa selecionar o método de pagamento desejado, inserir informações de entrega corretamente e receber uma confirmação de compra.

7. Gestão de pedidos: Verificar se os usuários podem acompanhar o status dos pedidos, visualizar histórico de compras, solicitar cancelamentos ou devoluções e fornecer feedback sobre as transações.

8. Avaliações e feedback: Validar se os usuários podem deixar avaliações e feedback para produtos e vendedores, garantindo que essas informações sejam exibidas corretamente e reflitam a reputação dos usuários.

```mermaid
    flowchart TD
        A[Registro de usuário] --> B(Login de usuário)
        B --> C{Pesquisar produtos}
        C --> D[Visualização de detalhes do produto]
        D --> E[Adição ao carrinho de compras]
        E --> F[Processo de checkout]
        F --> G[Gestão de pedidos]
        G --> H[Avaliações e feedback]
```

### Candidatos a automação

Teste de registro de usuário: Automatizar o processo de registro de novos usuários, garantindo que os dados sejam capturados corretamente. Valor gerado: economia de tempo e esforço dos testadores. Resultados esperados: verificação de dados corretos, envio de e-mails de verificação e redirecionamento para a página inicial após o registro bem-sucedido.

Teste de login de usuário: Automatizar o processo de login, verificando a autenticação correta e protegendo contra tentativas de acesso não autorizadas. Valor gerado: garantia de acesso seguro às contas dos usuários. Resultados esperados: autenticação adequada, redirecionamento para a página inicial após o login bem-sucedido e tratamento adequado de erros de login.

Teste de pesquisa de produtos: Automatizar a pesquisa de produtos e verificar resultados relevantes e precisos. Valor gerado: garantia de resultados satisfatórios da pesquisa. Resultados esperados: resultados de pesquisa corretos com base nos critérios, exibição correta dos produtos e consultas eficientes.

Teste de adição ao carrinho de compras: Automatizar a adição de produtos ao carrinho, verificando se são adicionados corretamente. Valor gerado: garantia de adição adequada de produtos. Resultados esperados: adição correta de produtos ao carrinho, atualização adequada do total e exibição correta dos produtos selecionados.

Esses testes automatizados ajudam a assegurar o funcionamento correto e eficiente da plataforma de e-commerce, melhorando a experiência do usuário e economizando tempo e recursos nos testes manuais.
