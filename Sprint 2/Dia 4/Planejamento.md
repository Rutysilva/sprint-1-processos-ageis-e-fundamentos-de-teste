# Plano de Teste - Um Mapa Essencial para Teste de Software

[Voltar a página inicial](https://gitlab.com/Rutysilva/sprint-1-processos-ageis-e-fundamentos-de-teste/-/blob/main/README.md)

Uma atividade essencial no desenvolvimento de todo e qualquer projeto é o planejamento. Um plano tem o papel semelhante ao de um ‘mapa’. Sem um mapa, um plano ou qualquer outra fonte de informação similar, você não conhecerá seus objetivos, nem aonde quer chegar e jamais terá a certeza de ter alcançado sua meta. Perceba que entender o propósito do planejamento é de suma importância a fim de monitorar a execução de atividades, sendo também importante conhecer o papel dos riscos no planejamento, bem como diferenciar estratégias de planos. Planejamento engloba três atividades principais:

1.     Definir um cronograma de atividades: estabelecer as atividades que devem ser realizadas, as etapas a serem seguidas e a ordem cronológica de execução;

2.     Fazer alocação de recursos: definir quem realiza as atividades e quais ferramentas/recursos a serem utilizados;

3.     Definir marcos de projeto – estabelecer os marcos, ou milestones, a serem alcançados com objetivo de se fazer o acompanhamento.

Perceba que o planejamento é acompanhado da atividade de monitoração ou supervisão que visa avaliar se o progresso que tem sido alcançado está em conformidade com o que foi estabelecido no plano ou, em outras palavras, responder a questão: quão bem estamos indo no projeto?

Agora, dentro do contexto do desenvolvimento de software, você necessitará de vários documentos como, por exemplo, plano de projeto, documento de requisitos e plano de teste. Neste artigo, o foco recai sobre o último, isto é, plano de teste. Trata-se de um documento ou mapa no qual se definem escopo e objetivos, além de requisitos, estratégias e recursos a serem empregados nas atividades de testes de software. Nesse sentido, o artigo apresenta os itens que devem fazer parte de um documento de plano de teste, exemplificando e discutindo esses itens.

###  Teste de Software

Teste de software é uma das atividades do processo de desenvolvimento de sistema de software que visa executar um programa de modo sistemático com o objetivo de encontrar falhas. Perceba que isto requer verificação e validação de software. Nesse sentido, definir quando as atividades de verificação e validação iniciam e terminam, como os atributos de qualidade serão avaliados e como os releases do software serão controlados, são questões que devem ser acompanhadas ao longo do processo de software.

Vale ressaltar que teste não deve ser a última atividade do processo de desenvolvimento de software. Ela ocorre durante todo o processo, como exemplificado na visão geral do processo RUP (Rational Unified Process) mostrado na Figura 1.

<p>
  <img src="https://www.devmedia.com.br/imagens/engsoft/ed15/artigo6/image.jpg" width="800" title="Imagem">
</p>
<b>Figura 1: </b> Visão geral do RUP.

E, além de encontrar falhas, testes objetivam aumentar a confiabilidade de um sistema de software, isto é, aumentar a probabilidade de que um sistema continuará funcionando sem falhas durante um período de tempo.

Embora seja desejável testar um sistema por completo, deve-se ter em mente que a atividade de teste assegura apenas encontrar falhas se ela(s) existirem, mas não asseguram sua ausência. Portanto, as atividades devem ser disciplinadas a fim de identificar a maioria dos erros existentes. Note que realizar os testes de software implica em responder às questões:

1.      Quais atributos da qualidade deverão ser testados?

2.      Quem realizará os testes?

3.      Quais recursos serão utilizados?

4.      Quais as dependências entre os atributos de qualidade?

5.      Quais as dependências entre as atividades de desenvolvimento?

6.      Como o processo e a qualidade do sistema de software serão acompanhados?

Na seção seguinte, um exemplo do conjunto de seções de um plano de teste é apresentado com o objetivo de ilustrar como as informações pertinentes ao teste de software poderiam ser tratadas e documentas. Não há, portanto, o objetivo de ser completo, pois cada sistema possui suas peculiaridades que devem ser consideradas caso a caso.

### Plano de Teste

O plano de teste é um dos documentos produzidos na condução de um projeto. Ele funciona como:

- Um ‘integrador’ entre diversas atividades de testes no projeto;

- Mecanismo de comunicação para os stakeholders (i.e. a equipe de testes e outros interessados);

- Guia para execução e controle das atividades de testes.

 

O plano de teste, que pode ser elaborado pelo gerente de projeto ou gerente de testes, visa planejar as atividades a serem realizadas, definir os métodos a serem empregados, planejar a capacidade necessária, estabelecer métricas e formas de acompanhamento do processo. Nesse sentido, deve conter:

- Introdução com identificação do projeto (definições, abreviações, referências), definição de escopo e objetivos;

- Conjunto de requisitos a serem testados;

- Tipos de testes a serem realizados e ferramentas utilizadas;

- Recursos utilizados nos testes;

- Cronograma de atividades (e definição de marcos de projeto).

Em outras palavras, um plano de teste deve definir:

1.      Os itens a serem testados: o escopo e objetivos do plano devem ser estabelecidos no início do projeto.

2.      Atividades e recursos a serem empregados: as estratégias de testes e recursos utilizados devem ser definidos, bem como toda e qualquer restrição imposta sobre as atividades e/ou recursos.

3.      Os tipos de testes a serem realizados e ferramentas empregadas: os tipos de testes e a ordem cronológica de sua ocorrência são estabelecidos no plano.

4.      Critérios para avaliar os resultados obtidos: métricas devem ser definidas para acompanhar os resultados alcançados.

Perceba que o planejamento é necessário a fim de antecipar o que pode ocorrer e, portanto, provisionar os recursos necessários nos momentos adequados. Isto significa coordenar o processo de teste de modo a perseguir a meta de qualidade do produto (sistema de software).

### Exemplificando o Plano de Teste

O plano de teste contém um conjunto de informações que permite ao gerente de projeto não apenas coordenar as atividades de testes de um projeto, mas também monitorar seu progresso e verificar se o executado está em conformidade com o planejado. A Tabela 1 apresenta uma relação dos itens consideradas imprescindíveis em um plano de teste. A relação de itens não pressupõe a intenção de ser completo, mas de apontar os itens considerados como obrigatórios num plano de teste de uma instituição.

<b> Itens de um plano de teste </b>
1. Introdução
Contém uma identificação do projeto, descrição dos objetivos do documento, o público ao qual ele se destina e escopo do projeto a ser desenvolvido. Pode adicionalmente conter termos e abreviações usadas, além de informar como o plano deve evoluir.

2. Requisitos a serem testados
Esta seção descreve em linhas gerais o conjunto de requisitos a serem testados no projeto a ser desenvolvido, comunicando o que deve ser verificado. Exemplos de requisitos a serem testados são: desempenho, segurança, interface de usuário, controle de acesso, funcionalidades.

3. Estratégias e ferramentas de teste
Apresenta um conjunto de tipos de testes a serem realizados, respectivas técnicas empregadas e critério de finalização de teste. Além disso, é listado o conjunto de ferramentas utilizadas.

4. Equipe e infra-estrutura
Contém descrição da equipe e da infra-estrutura utilizada para o desenvolvimento das atividades de testes, incluindo: pessoal, equipamentos, software de apoio, materiais, dentre outros. Isto visa garantir uma estrutura adequada para a execução das atividades de testes previstas no plano.

5. Cronograma de atividades
Contém uma descrição de marcos importantes (milestones) das atividades (incluindo as datas de início e fim da atividade). Apenas marcos relevantes devem ser listados, ou seja, aqueles que contribuirão nas atividades de testes. Por exemplo: projeto de testes, execução de testes ou avaliação de testes.

6. Documentação complementar
Apresenta-se uma relação dos documentos pertinentes ao projeto.
