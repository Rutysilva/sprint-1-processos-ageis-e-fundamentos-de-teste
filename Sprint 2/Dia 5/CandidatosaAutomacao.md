# Testes candidatos à automação

A automação de testes é uma área em franca expansão, no entanto, é uma área ainda muito imatura. Muitos dos sucessos nos projetos de automação de testes são decorrentes de processos empíricos de tentativa e erro.

Para piorar a situação, a automação de testes ainda é objeto de mitos que geram percepções errôneas sobre os seus reais benefícios e limitações. Frequentemente, as ferramentas de automação de testes são supervalorizadas gerando falsas expectativas, a infra-estrutura requerida é subestimada, entre outros problemas comuns que serão apresentados ao longo deste artigo.

Melhores Práticas na Automação de Testes:
- A automação de testes não é um processo de testes
- Automatize os testes críticos primeiro
- Incorpore testabilidade ao aplicativo
- As ferramentas de automação de testes também têm defeitos
- Demo não é prova de conceito
- Dimensione a infra-estrutura adequadamente
- Encare a automação de testes como um projeto
- Alinhe as expectativas e garanta a colaboração de todos os envolvidos
- A automação de testes é um investimento de longo prazo
- O teste manual é insubstituível

### A automação de testes não é um processo de testes

Muitas empresas fracassam na implantação da automação de testes em virtude da inversão de prioridades causada pela busca da qualidade a qualquer preço. Neste cenário, as empresas adquirem uma ferramenta de automação de testes prematuramente sem, ao menos, possuírem um grau mínimo de maturidade no processo de testes. O processo de testes é informal, as responsabilidades não são bem definidas e os profissionais não possuem o perfil adequado ou não são qualificados.

Segundo Watts S. Humphrey, criador do CMM (Capability Maturity Model), a qualidade do produto final é diretamente proporcional à qualidade do processo utilizado no seu ciclo de vida. A implantação da automação de testes depende de testes manuais maduros e consistentes. Os projetos de automação de testes bem sucedidos são aqueles que se baseiam em processos de testes formais e estruturados. Afinal, como é possível esperar alguma coisa de testes automatizados que são baseados em testes manuais errados, ambíguos ou inconsistentes. Não é possível automatizar o caos.

A rigor, o sucesso na implantação da automação de testes depende do entendimento de que a automação de testes ou uma ferramenta de automação, por si só, não vão melhorar ou organizar os testes existentes. É necessário antes, fazer a “faxina” e implantar um processo de testes formal. A necessidade de automatizar os testes virá naturalmente como resultado da evolução da maturidade do processo de testes.

### Automatize os testes críticos primeiro

Nove em cada dez projetos de automação de testes cometem o erro de transformar todos os casos de testes manuais em scripts de testes automatizados. O leitor deve estar se perguntando: Mas este não seria o objetivo principal da automação de testes? – Sim e Não.

A automação de testes tem o objetivo de reduzir o envolvimento humano em atividades manuais repetitivas. Entretanto, isto não significa que a automação de testes deve se limitar apenas a fazer o trabalho repetitivo e chato. Os casos de testes manuais foram criados num contexto onde os testes seriam executados manualmente. É muito provável que estes casos de testes manuais não sejam muito eficientes em um contexto onde os testes serão executados automaticamente.

Freqüentemente, antes de iniciar a automação, os testes devem ser re-projetados a fim de aumentar a probabilidade de revelar um defeito que ainda não tenha sido encontrado. Afinal, o grande benefício da automação de testes não é a execução dos testes mais rápida e a qualquer hora do dia ou da noite, mas o aumento da amplitude e profundidade da cobertura dos testes.

Na prática, a automação de 100% dos testes manuais nem sempre é a melhor estratégia. A automação de testes é pouco eficaz quando os testes são complexos e exigem interações intersistemas ou validações subjetivas (estes tipos de testes devem permanecer manuais). Além disso, muitas vezes o custo e o tempo para automatizar os testes de um projeto são maiores que o custo e o tempo do próprio projeto de desenvolvimento (o que inviabilizaria a automação de 100% dos testes manuais).

A escolha dos testes automatizados candidatos, ou seja, os mais críticos, deve ser realizada com base no contexto do projeto de automação. No entanto, apesar de não existir uma categorização amplamente difundida, a experiência tem mostrado que os testes candidatos são normalmente agrupados em quatro áreas distintas:

- Smoke Tests: Um conjunto mínimo de testes é selecionado com o objetivo de validar um Build ou liberação antes do início de um ciclo de testes;

- Testes de Regressão: Os testes são selecionados com o objetivo de executar o re-teste de uma funcionalidade ou da aplicação inteira;

- Funcionalidades Críticas: Os testes são selecionados com o objetivo de validar as funcionalidades críticas que podem trazer riscos ao negócio;

- Tarefas Repetitivas: Os testes são selecionados com o objetivo de reduzir o envolvimento dos testadores em atividades manuais repetitivas e suscetíveis a erros, tais como cálculos matemáticos, simulações, processamentos, comparações de arquivos ou dados, etc.

### Incorpore testabilidade ao aplicativo
Via de regra, os testes são automatizados utilizando a aplicação do jeito que ela é. Ou seja, os testes são criados sob o ponto de vista da interface gráfica e com o único objetivo de automatizar as ações dos usuários finais.

No entanto, testar por meio da interface gráfica nem sempre é a melhor opção para testes que exigem desempenho. Além disso, às vezes a interface gráfica não fornece evidências de que o teste foi realizado com sucesso, afinal, nem sempre a mensagem “Essa operação foi realizada com sucesso” significa que a operação foi realizada com sucesso de verdade.

A experiência tem mostrado que a incorporação de testabilidade na aplicação é um fator chave para o sucesso de um projeto de automação de testes. A testabilidade é um atributo que determina a capacidade de uma aplicação ser testada, ou seja, a facilidade de se testar uma aplicação está diretamente ligada ao nível de testabilidade incorporado nesta aplicação.

Normalmente é necessário realizar modificações na aplicação para torná-la mais fácil de testar. Essas modificações têm o objetivo de incorporar um conjunto de mecanismos que facilitam a observação e o controle do estado dos componentes internos da aplicação. Adicionalmente, estes mecanismos expõem ao mundo exterior as funcionalidades da aplicação por meio de APIs, Interfaces de Linha de Comando, Hooks, etc.

Isto torna, por sua vez, a aplicação muito mais fácil de se testar, sob o ponto de vista da automação de testes. O objetivo desses mecanismos (APIs, Interfaces de Linha de Comando, Hooks) é fornecer interfaces para o mundo exterior que não seja dependente da interface gráfica da aplicação. Dessa forma, é possível criar testes especializados para exercitar algumas funcionalidades da aplicação sem que seja necessário utilizar uma interface gráfica.

# A automação de testes é um investimento de longo prazo

A automação de testes, sem dúvida, é uma boa prática em um processo de teste de software. No entanto, a automação de testes não é uma prática que se adota do dia para a noite. Conforme mencionamos anteriormente, a necessidade de automatizar os testes virá naturalmente como resultado da evolução da maturidade do processo de testes.

Apesar de todos os benefícios advindos da automação de testes, os investimentos são altos. Rex Black, guru na área de testes de software, no seu famoso artigo chamado “Successful Investing in Software Testing” apresenta um cenário hipotético com os custos requeridos para realizar testes informais, manuais e automatizados e o retorno de investimento (ROI) obtido em cada uma dessas abordagens.