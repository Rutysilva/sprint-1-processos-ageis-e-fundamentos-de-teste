# Swagger

<p>
  <img src="https://3025166959-files.gitbook.io/~/files/v0/b/gitbook-legacy-files/o/assets%2F-MgwMOcm4UygF4VcCNIq%2F-Mj_A8lxzMa4XaT3D1mT%2F-Mj_AvIoKnZa-1neamkq%2Fswagger.png?alt=media&token=b883b913-6afc-4de3-9ec4-c5a09d8081c8" width="800" title="Imagem">
</p>

Trata-se de uma aplicação open source que auxilia desenvolvedores nos processos de definir, criar, documentar e consumir APIs REST.  Em suma, o Swagger visa padronizar este tipo de integração, descrevendo os recursos que uma API deve possuir, como endpoints, dados recebidos, dados retornados, códigos HTTP e métodos de autenticação, entre outros.

Ele simplifica o processo de escrever APIs, especificando os padrões e fornecendo as ferramentas necessárias para escrever APIs seguras, com alto desempenho e escaláveis.

No mundo do software de hoje, não há sistemas rodando on-line sem expor uma API. Passamos de sistemas monolíticos para microsserviços. E todo o design de microsserviços é baseado em APIs REST.

APIs REST são frequentemente usadas para a integração de aplicações, seja para consumir serviços de terceiros, seja para prover novos. Para estas APIs, o Swagger facilita a modelagem, a documentação e a geração de código.
Como é uma das ferramentas mais usadas para esta finalidade, a SmartBear Software, empresa que a gerencia, criou a Open API Iniciative e renomeou as especificações do Swagger para OpenAPI Specification.

Desse modo, atualmente, a Open API Iniciative trabalha para criar, evoluir, engajar e promover um formato de especificação de APIs open source baseado em Swagger, amplamente usado por produtores de interfaces de integração e fornecedores neutros de softwares comerciais.

