# API Pública
Os formatos mais comuns de APIs são as públicas ou abertas. As APIs Públicas são as responsáveis por sites que incorporam funções de redes sociais, como curtir e compartilhar e permitem acesso direto a pequenas funcionalidades da plataforma principal. APIs públicas normalmente são disponibilizadas gratuitamente na internet para que desenvolvedores consigam integrar suas plataformas e aproveitar algumas vantagens que elas oferecem. 

O uso de logins através de perfis no Facebook e Google+, por exemplo, faz com que muitos aplicativos consigam trabalhar com as mesmas listas de amigos das redes sociais e consigam integrar seus usuários sem que seja necessário criar uma plataforma interna para realizar a mesma função.

As principais vantagens da API pública são:

- Incentiva desenvolvedores a trabalharem com a sua plataforma;
- Facilita a criação de funcionalidades integradas com um grande número de usuários;
- Não exige que a empresa fornecedora desenvolva APIs específicas para parceiros;
- Alavanca o uso de novas plataformas através da integração imediata com outras existentes;
- Trabalhar com uma API pública gera dados importantes sobre seus usuários.

## Exemplo de API's públicas

1. [The Open Movie Database (OMDb)](https://www.omdbapi.com/)
O OMDb contém diferentes tipos de filmes que você pode usar em seus projetos.

2. [Pokemon API](https://pokeapi.co/)
Você provavelmente se lembraria dessas criaturas incríveis que podem fazer coisas incríveis. Agora você pode usá-los em seus projetos.

3. [Rick & Morty API](https://rickandmortyapi.com/)
Esses dois personagens realmente nos ensinaram algo. Honre-o usando em seus projetos.

4. [ADiceBear Avatar API](https://www.dicebear.com/)
Se você quer gerar avatares bonitos e exclusivos, verifique a documentação do diceBear.

5. [New York Tİmes](https://developer.nytimes.com/)
Receba as últimas notícias do mundo de forma rápida e prática.

7. [Fixer](https://fixer.io/)
Veja a atual taxa de câmbio entre as diferentes moedas.
