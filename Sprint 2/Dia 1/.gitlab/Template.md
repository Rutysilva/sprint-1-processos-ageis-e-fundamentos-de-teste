## Relatório de Bug - Modelo 

### Título do Bug:
-     Insira um breve título que descreva o problema encontrado

### Descrição do Bug:
-     Forneça uma descrição detalhada do bug encontrado, incluindo todos os passos necessários para reproduzi-lo. Se possível, anexe capturas de tela, mensagens de erro ou qualquer outra informação relevante que possa ajudar a entender o problema.

### Comportamento Esperado:
-     Descreva qual é o comportamento correto que a aplicação deveria apresentar.

### Comportamento Observado:
-     Descreva o comportamento real que você observou, o qual está em desacordo com o comportamento esperado.

### Passos para Reproduzir o Bug:
-     Enumere os passos necessários para reproduzir o bug, incluindo detalhes específicos, como cliques, inserção de dados, etc.

### Ambiente de Teste:
-     Forneça informações detalhadas sobre o ambiente de teste, como sistema operacional, versão do navegador, dispositivos utilizados, etc.

### Prioridade do Bug:
-     Classifique a prioridade do bug com base em sua gravidade e impacto no funcionamento da aplicação. Por exemplo: alta, média, baixa.

### Sugestões de Reparo:
-     Caso tenha alguma sugestão sobre como corrigir o bug ou ideias de como investigá-lo, inclua essas informações aqui.

### Informações do Relator:
-     Forneça suas informações de contato, como nome e e-mail, caso seja necessário entrar em contato para obter mais detalhes ou atualizações sobre o bug relatado.

### Observações Adicionais:
-     Adicione quaisquer observações relevantes que possam auxiliar na compreensão ou resolução do bug.