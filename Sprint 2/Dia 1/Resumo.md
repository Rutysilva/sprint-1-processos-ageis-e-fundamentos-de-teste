# Conceitos HTTP, API REST, JSON, User Stories & Issues


<p >
  <img src="https://appmaster.io/cdn-cgi/image/width=1024,quality=83,format=auto/api/_files/PqV7MuNwv89GrZvBd4LNNK/download/" width="800" title="api">
</p>

### O que é uma API?

A sigla API ou 'Interface de Programação de Aplicações' refere-se a serviços que são uma forma de integrar sistemas, possibilitando
benefícios como a segurança dos dados, facilidade no intercâmbio entre informações com diferentes linguagens de programação e a
monetização de acessos.

### Vantagens de usar uma API

1. Ajuda na reestruturação de sistemas internos
O uso de APIs nas empresas ajuda a melhorar a eficiência operacional por meio da automação de processos. Isso significa dizer que essa tecnologia favorece à criação de workflows mais eficazes, que impactam positivamente na produtividade de sua equipe.

2. Melhora a experiência do usuário
O uso de APIs nas empresas não se reduz apenas à automação. Elas também favorecem à customização de processos, o que permite que os usuários tenham uma transição mais fluida entre as diferentes aplicações utilizadas.

Quando bem projetadas, as APIs são desenvolvidas para atender exatamente às necessidades dos usuários, muitas vezes de diferentes departamentos, que utilizam diferentes aplicações.

Dessa forma, os colaboradores podem ter acesso às informações produzidas em outra ferramenta a partir daquela com o qual eles já possuem experiência. Isso não só facilita a interpretação dos dados, mas também melhora a produtividade.

3. Permite a conectividade com parceiros
O uso de APIs também pode fornecer uma maneira consistente de trocar dados com empresas parceiras. Se você possui, por exemplo, um controle de estoque terceirizado, por meio de uma API você pode fornecer uma integração sólida e segura de informações.

No passado, a solução mais utilizada nesses casos era o envio de planilhas. Esse método, no entanto, está propenso a erros e retrabalhos. Se, ao importar uma planilha, um campo requerido pelo sistema estiver ausente, ou até mesmo uma vírgula fora de lugar, isso pode custar horas de esforço e atrasar os processos importantes.

4. Oferece novas oportunidades de interação com os clientes
Se você possui algum canal virtual de interação com seus clientes, então você não pode desconsiderar o poder do uso das APIs nas empresas.

Atualmente, 62% da população brasileira está ativa nas redes sociais, segundo relatório intitulado “Digital in 2018: The Americas” e divulgado pelas empresas We are Social e Hootsuite. Mas, o que isso tem a ver com APIs? Tudo!

As APIs públicas mais utilizadas nos últimos anos são relacionadas às redes sociais e são oferecidas, por exemplo, pelo Facebook, Instagram, Google, etc.

Essas APIs são utilizadas, principalmente, para fazer cadastro em sites, portais e aplicativos, utilizando as informações pessoais vinculadas às redes sociais. Isso torna o processo muito mais ágil e simples, o que favorece à satisfação do cliente.

Além disso, muitas pessoas desistem de entrar em contato com uma empresa quando é necessário cadastro. Ao utilizar uma API, você terá muito menos chance de desistência, em virtude da praticidade da ação.

### O que é HTTP?

O Hypertext Transfer Protocol, sigla HTTP é um protocolo de comunicação utilizado para sistemas de informação de hipermídia, distribuídos e colaborativos. Ele é a base para a comunicação de dados da World Wide Web. Hipertexto é o texto estruturado que utiliza ligações lógicas entre nós contendo texto.

### REST & RESTful
<p align ="center">
  <img src="https://i.ytimg.com/vi/7Cbd8WBGrq4/maxresdefault.jpg" width="800" title="rest">
</p>

- REST (Representational State Transfer) é um estilo de arquitetura que define padrões que facilitam a comunicação entre sistemas via
Web, permite que o cliente e o servidor sejam implementados independentemente, sem a necessidade que um tenha conhecimento
da implementação do outro. Usa o protocolo HTTP e é baseado em contrato.

- RESTful possui os mesmos princípios de REST. A diferença é que a API implementada precisa estar de acordo com todas regras e
restrições definidas para a construção de APIs REST. Em resumo a API precisa ter um grau de maturidade alto para cumprir todos os
critérios de uma API REST definidos porRoy Fielding

### Json

JSON é basicamente um formato leve de troca de informações/dados entre sistemas. Mas JSON significa JavaScript Object Notation, ou seja, só posso usar com JavaScript correto? Na verdade não e alguns ainda caem nesta armadilha.

O JSON além de ser um formato leve para troca de dados é também muito simples de ler.

Vantagens do JSON:
-Leitura mais simples
-Analisador(parsing) mais fácil
-JSON suporta objetos! Sim, ele é tipado!
-Velocidade maior na execução e transporte de dados
-Arquivo com tamanho reduzido
-Quem utiliza? Google, Facebook, Yahoo!, Twitter...

### User Stories

<p align ="center">
  <img src="https://blog.brunoguazina.com/wp-content/uploads/2020/02/UserHistory-Exemplo-1024x341.jpg" width="800" title="user">
</p>

Podemos definir e organizar os requisitos de um sistema utilizando User Stories (histórias de usuário). User Stories são artefatos de desenvolvimento utilizados em sistemas geridos segundo metodologias ágeis.

Muitas perguntas são levantadas na hora de escrever User Stories, por exemplo:

     * User Stories são iguais Casos  de Uso?

     * Como descrevo minhas User Stories?

     * Que tipo de informações podemos inserir nas User Stories?

     * De quem e pra quem são feitas?

As respostas para essas perguntas são amplamente discutidas na comunidade Scrum.

Casos de Uso e User Stories são similares, como é apresentado por Martin Fowler em seu texto Use Cases and User Stories. Ambos são utilizados para organizar requisitos. Porém, enquanto Casos de Uso descrevem ações de interação segundo uma narrativa impessoal entre o usuário e o sistema, User Stories focam nos objetivos do usuário e como o sistema alcança esses objetivos.

User Stories fracionam os requisitos para que seja possível (e mais fácil) estimar o esforço para realizar aquele objetivo. Resumindo, User Stories são descrições simples que descrevem uma funcionalidade e é recomendável que sejam escritas segundo o ponto de vista do usuário.

User Stories devem ser curtas, simples e claras. Devemos conseguir escrevê-las em um simples e pequeno cartão (conhecidos como User Index Cards). Se não há espaço para escrevê-la em um cartão é porquê devemos refiná-la mais, e as dividir em outras User Stories.

Ator – O proprietário da User Story. De forma simplista é o usuário, o interessado naquela funcionalidade. Mas é recomendado descrever de forma específica quem é o ator para ser mais fácil identificar o contexto da história dentro do sistema.

Ação – É o que o ator quer fazer. Utilizando aquela ação ele espera alcançar seu objetivo dentro do sistema.

Funcionalidade – É o que o ator espera que aconteça ao realizar a ação. Ou seja, é o resultado de executar a ação segundo a ótica do ator. Também pode ser visto como justificativa.

Alguns aspectos importantes podem ser notados no uso de User Stories:

Validar se a funcionalidade é realmente necessária antes de incluí-la
Análise das necessidades reais do usuário
Ajuda a priorizar o que deve ser feito
É mais fácil estimar o esforço que será necessário para implementar a funcionalidade

### Issues

O Issue é uma ferramenta interessante do GitHub para deixar um feedback ou auxiliar na construção de algum projeto desenvolvido por outras pessoas. Trata-se da ideia de deixar comentários em projetos. Com ela, é possível escrever textos, colocar imagens, usar alguns métodos de marcação, colocar links e postar "emojis" para fortificar a sua issue. Com esse recurso, os programadores ativos conseguem aumentar sua networking participando em outros projetos. Não é a toa que o GitHub também é respeitado por ser uma verdadeira rede social profissional.

Há duas possibilidades de se usar uma issue: a interação por parte dos donos do projeto e a interação por parte dos colaboradores. Vamos ver abaixo como ocorre os procedimentos nas duas situações.