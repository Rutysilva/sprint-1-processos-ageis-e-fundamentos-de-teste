# Falando sobre erros e categorias de testes

## Categorias de teste

* Funcionais
* Não funcionais 
* Estruturais
* Segurança 

## Erros

Analisando os erros devemos avaliar:
<b> O Nível de gravidade</b>
Se o bug é muito grave, causará diversos danos ao programa. A partir dele é possível identificar quando ou que tipo de dano será causado e como será solucionado.
<b>Qual a prioridade</b>
Identifica quão rápido ou quão perfeitamente o bug é erradicado. Avalia quão prejudicial é o bug e de como é importante corrigi-lo.
<b>Risco</b>
Qualquer evento improvável/incerto com impacto positivo/negativo no projeto. Esses eventos podem chegar a afetar o custo, a qualidade e pontualidade da entrega.

## Tipos de erros e como prevenir

* <b>Causados por documentação</b> -> Atentar para que a documentação do projeto esteja correta, sem divergências que impessam que ele funcione corretamente;
* <b>Causados por massa de dados</b> -> Identificar préviamente se os limites operacionais estão bem definidos.
* <b>Segurança</b> -> Para evitar erros deste tipo, é necessário realizar testes constantes, além de seguir protocolos atualizados e seguros que estejam em conformidade com leis, como a LGPD. Ademais, é imprescindível buscar atualizações sobre novas formas mais eficazes de realizar a proteção de dados, autenticação e autorização. Há necessidade de manter as bibliotecas do seu projeto nas versões mais atuais, as quais ainda não possuem vulnerabilidades. O estudo sobre aspectos não programáticos como protocolos e leis devem ser sempre feitas em conjunto de especialistas das respectivas áreas.
* <b>Regra de negócio mal definida</b> -> É necessário garantir um bom conhecimento dos requisitos a equipe responsável, além de uma comunicação efetiva  e validação de regras de negócio com stakeholders.
* <b>Solicitações com informações insuficientes</b> -> Uma solicitação HTTP com falta de informações pode gerar inúmeros problemas, como, por exemplo, a outra parte não saber em que formato deve enviar uma resposta. Para prevenir esse tipo de erro, é necessário se atentar a detalhes e seguir boas práticas.
* <b>Erros estruturais</b> -> É importante que nossa API esteja bem estruturada para facilitar o processo de desenvolvimento, assim nós vamos garantir uma melhor qualidade de entrega, além de aumentar a produtividade do time de desenvolvimento. 
* <b>Erros de compatibilidade</b> -> Esses erros são causados por questões de ambiente de execução, então nós temos que garantir que a aplicação vai funcionar em diferentes Sistemas Operacionais (se for nescessário).

## Equipe

* Artur Dantas Rodrigues
* Ellen Santos Ramalho
* Renato Davoli Moreira
* Rogerio Antônio Augusto
* Ruty da Silva Cardoso
    

## Links externos
[Teste de API: o que é, como fazer e boas práticas
](https://www.lucidchart.com/blog/pt/teste-de-api-guia-completo)