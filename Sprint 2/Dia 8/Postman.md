# O que é Postman?

O Postman é uma ferramenta que dá suporte à documentação das requisições feitas pela API. Ele possui ambiente para a documentação, execução de testes de APIs e requisições em geral.

Ao utilizá-lo, você passará a trabalhar com APIs de modo mais eficiente, construindo solicitações rapidamente e, ainda, poderá guardá-las para uso posterior, além de conseguir analisar as respostas enviadas pela API.

Um bom motivo para usar essa ferramenta é que, por meio dela, é possível reduzir drasticamente o tempo necessário para testar e desenvolver APIs.

Em um exemplo prático, imagine que você queira fazer uma solicitação GET para procurar certas informações no nome da empresa.

Se fosse o caso de testar uma solicitação GET sem usar o Postman, você precisaria escrever todo um código para executar a requisição, além de uma interface visual para interagir com essa rotina.

Se fosse concedido, provavelmente você precisaria escrever tudo isso para criar um aplicativo funcional usando essa API, mas todo esse trabalho seria simplesmente para testar a sua funcionalidade, o que de fato, nesse formato, é tedioso e demorado.

Já com o Postman, esse teste é muito mais otimizado. Tudo o que precisa ser feito é:

- Obter a conexão da rota à barra de endereços;
- Selecionar o método de resposta GET na caixa suspensa à esquerda;
- Digitar a chave da API na seção "Headers";
- Especificar o formato da resposta, que poderia ser em JSON, por exemplo, e;
- Clicar em enviar.

Em seguida, será obtido os dados de resposta em JSON (simples e fácil de ler) acompanhado do código de status 200, que confirma que a solicitação GET foi bem-sucedida. Bem mais tranquilo, não?

### Por que usar o Postman?

Além de ser um aplicativo gratuito e fácil de aprender, com pouco tempo você já estará enviando seus primeiros requests (solicitações/requisições). No mais, trata-se de uma ferramenta com um amplo suporte para todas as APIs e Schemas.

Entre as principais vantagens em se usar o Postman, podemos destacar:

- Possibilita qualquer tipo de chamada de API —  REST, SOAP ou HTTP;
- Oferece suporte para formatos de dados populares, como OpenAPI GraphQL e RAML;
- Fácil acessibilidade: Para usar o Postman, basta fazer login em sua conta, facilitando o acesso a arquivos a qualquer momento, em qualquer lugar, desde que um aplicativo Postman esteja instalado no computador;
- Uso de Coleções: O Postman permite que os usuários criem coleções para suas chamadas de API. Cada coleção pode criar subpastas e várias solicitações. Isso ajuda a organizar suas suítes de teste;
- Colaboração (Collaboration): Coleções e ambientes podem ser importados ou exportados, facilitando o compartilhamento de arquivos. Um link direto também pode ser usado para compartilhar coleções;
- Criar ambientes: Ter vários ambientes ajuda a ter menos repetições de testes, pois é possível usar a mesma coleção, mas em um ambiente diferente;
- Elaborar testes: Pontos de verificação de teste, como a verificação do status de resposta HTTP bem-sucedido, podem ser adicionados a cada chamada de API, o que ajuda a garantir a cobertura do teste;
- Teste de automação: Com o uso do Collection Runner ou Newman, os testes podem ser executados em várias iterações, economizando tempo para testes repetitivos;
- Depuração: O console do Postman ajuda a verificar quais dados foram recuperados, facilitando a depuração de testes;
- Integração Contínua: A partir da sua capacidade de oferecer suporte à integração contínua, são mantidas práticas de desenvolvimento.
