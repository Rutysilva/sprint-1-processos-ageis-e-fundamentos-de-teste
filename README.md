
# Repositório Compass UOL
![Compass](https://stc.uol.com/g/sobreuol/images/para-seu-negocio/compass-logo.svg?v=3.9.33)

Repositório utilizado para documentação das Sprints realzadas durante o estágio na Compass UOL - AWS for Software Quality & Test Automation

[Sprint 1](https://gitlab.com/Rutysilva/sprint-1-processos-ageis-e-fundamentos-de-teste/-/tree/main/Sprint%201)

[Sprint 2](https://gitlab.com/Rutysilva/sprint-1-processos-ageis-e-fundamentos-de-teste/-/tree/main/Sprint%202)

[Sprint 3](https://gitlab.com/Rutysilva/sprint-1-processos-ageis-e-fundamentos-de-teste/-/tree/pb_sprint3/Sprint%203)

#### Autor
[Ruty da Silva Cardoso](https://gitlab.com/Rutysilva)
#### Colaborador
[Maria Cristiane da Silva Torres](https://gitlab.com/Cristiane777)