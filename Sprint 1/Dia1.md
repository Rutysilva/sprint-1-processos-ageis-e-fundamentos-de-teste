# Dia 1 :: Ágil
## - Onboard;
## - Challenge;
## - Entregáveis;
## - Orientações Gerais.

Git é um sistema de controle de versão de software que permite que várias pessoas trabalhem em um mesmo projeto ao mesmo tempo, mantendo um histórico de todas as mudanças feitas no código. Isso ajuda a garantir que as mudanças não sejam sobrescritas ou perdidas e permite que os desenvolvedores trabalhem em paralelo em diferentes partes do código. Git é amplamente utilizado na indústria de desenvolvimento de software e é considerado uma ferramenta fundamental para desenvolvedores.

GitLab é uma plataforma de gerenciamento de projetos que utiliza o Git como sistema de controle de versão. Ele oferece recursos adicionais, como integração contínua, monitoramento de desempenho e gerenciamento de tickets de problemas. O GitLab é usado por equipes de desenvolvimento para gerenciar e colaborar em projetos de software, permitindo que as equipes trabalhem juntas de forma mais eficiente.

A Matriz de Eisenhower é uma ferramenta de gerenciamento de tempo que ajuda as pessoas a priorizar tarefas de acordo com sua importância e urgência. As tarefas são classificadas em quatro categorias: importantes e urgentes, importantes mas não urgentes, urgentes mas não importantes e não urgentes e não importantes. A matriz ajuda a definir prioridades e garantir que as tarefas importantes sejam concluídas em tempo hábil, enquanto as tarefas menos importantes não realizadas os prazos.