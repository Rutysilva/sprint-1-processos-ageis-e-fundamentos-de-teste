# Dia 7 :: MasterClass
## AWS 101
### Ambiente de TI clássico vs. Ambiente de Nuvem

Em um ambiente de TI clássico, as responsabilidades são tipicamente divididas entre as equipes de desenvolvimento e operações. As responsabilidades principais são as seguintes:

1. Desenvolvimento:
   - Projeto e desenvolvimento de aplicativos e sistemas.
   - Manutenção e atualização de software.
   - Testes de aplicativos.
   - Implementação de requisitos de negócios.
   
2. Operações:
   - Gerenciamento de servidores físicos.
   - Configuração e manutenção de redes.
   - Gerenciamento de armazenamento e backup.
   - Monitoramento de sistemas.
   - Resolução de problemas e suporte técnico

Em contraste, em um ambiente de nuvem, as responsabilidades são distribuídas de forma diferente, pois muitas tarefas de infraestrutura são automatizadas e gerenciadas pelo provedor de nuvem. As responsabilidades principais são as seguintes:

1. Desenvolvimento:
   - Design e desenvolvimento de aplicativos nativos da nuvem.
   - Implementação e gerenciamento de microsserviços.
   - Integração contínua e entrega contínua (CI/CD).
   - Monitoramento e otimização de desempenho.

2. Operações:
   - Configuração e gerenciamento da infraestrutura na nuvem.
   - Monitoramento e escalabilidade automática.
   - Gerenciamento de segurança na nuvem.
   - Resolução de problemas relacionados à nuvem.
   - Implementação e gerenciamento de políticas de backup e recuperação.

### Infraestrutura como Código (IaC) e seus benefícios

A Infraestrutura como Código é uma prática em que a infraestrutura de TI é gerenciada por meio de código, usando linguagens de programação e ferramentas específicas. Os benefícios da IaC incluem:

1. Automatização: A IaC permite automatizar a criação, configuração e implantação de infraestrutura, tornando o processo mais rápido e consistente.

2. Consistência: Com a IaC, é possível garantir que a infraestrutura seja reproduzida exatamente da mesma forma em diferentes ambientes, eliminando discrepâncias e erros humanos.

3. Versionamento e controle de alterações: O código da infraestrutura pode ser versionado e controlado, permitindo rastrear alterações, reverter para versões anteriores e colaborar de forma mais eficiente.

4. Flexibilidade e escalabilidade: A IaC facilita a escalabilidade da infraestrutura, permitindo a adição ou remoção de recursos de forma rápida e controlada.

### Decisão entre equipes de desenvolvimento e operações

Tradicionalmente, as equipes de desenvolvimento e operações trabalhavam de forma separada, o que levava a problemas de comunicação e colaboração. No entanto, com a adoção de práticas ágeis e DevOps, há uma tendência crescente de integração entre essas equipes.
A abordagem ideal é promover a colaboração entre as equipes, criando times multifuncionais que reúnem desenvolvedores e profissionais de operações. Esses times trabalham juntos desde o início do ciclo de desenvolvimento até a implantação e operação contínua do sistema, compartilhando responsabilidades e conhecimento.

Essa abordagem proporciona uma visão holística do desenvolvimento e operações, permitindo a antecipação de problemas, a implementação de melhorias contínuas e a entrega mais rápida e confiável de software.

### Modelos de implantação da computação em nuvem

Ao selecionar uma estratégia de nuvem, a empresa deve considerar fatores como componentes de aplicativos de nuvem necessários, ferramentas de gerenciamento de recursos preferenciais e requisitos de infraestrutura de TI legada.

### Amazon Elastic Compute Cloud (Amazon EC2)

O Amazon Elastic Compute Cloud (Amazon EC2) fornece capacidade computacional segura e redimensionável na nuvem como instâncias do Amazon EC2.

Imagine que você é responsável pela arquitetura dos recursos de sua empresa e precisa ser compatível com novos sites. Com recursos locais tradicionais, você precisa fazer o seguinte:

•	Gastar dinheiro adiantadamente para comprar hardware.
•	Aguardar até que os servidores sejam entregues.
•	Instalar os servidores em seu data center físico.
•	Fazer todas as configurações necessárias.

Em comparação, com uma instância do Amazon EC2, você pode usar um servidor virtual para executar aplicativos na nuvem AWS.

•	Você pode provisionar e executar uma instância do Amazon EC2 em minutos.
•	Você pode parar de usar a instância quando terminar de executar uma carga de trabalho.
•	Você paga apenas pelo tempo de computação em que uma instância estiver em execução, não quando ela é interrompida ou encerrada.
•	Você pode economizar custos pagando apenas pela capacidade do servidor necessária ou desejada.

### Os tipos de instâncias do Amazon EC2

Os tipos de instâncias do Amazon EC2 são otimizados para tarefas diferentes. Ao selecionar um tipo de instância, considere as necessidades específicas de suas cargas de trabalho e seus aplicativos. Isso pode incluir requisitos para recursos de computação, memória ou armazenamento.

### Definição de preços do Amazon EC2

Com o Amazon EC2, você paga apenas pelo tempo de computação que usar. O Amazon EC2 oferece diversas opções de preço para diferentes casos de uso. Por exemplo, se o seu caso de uso tolerar interrupções, você poderá economizar com instâncias spot. Você também pode economizar assumindo um compromisso antecipadamente e bloqueando um nível mínimo de uso com instâncias reservadas.

### Escalabilidade

A escalabilidade envolve começar apenas com os recursos de que você precisa e projetar sua arquitetura para responder automaticamente às alterações de demanda, fazendo aumentos ou reduções. Como resultado, você paga apenas pelos recursos que usa. Você não precisa se preocupar com a falta de capacidade de computação para atender às necessidades de seus clientes.

Se você quisesse que o processo de scaling acontecesse automaticamente, qual serviço AWS você usaria? O serviço AWS que fornece essa funcionalidade para instâncias do Amazon EC2 é o Amazon EC2 Auto Scaling.

### Amazon EC2 Auto Scaling
Se você já tentou acessar um site que não carregava e atingiu o tempo limite algumas vezes, ele pode ter recebido mais solicitações do que conseguia atender. Essa situação é semelhante a esperar em uma longa fila em uma cafeteria quando há apenas um barista disponível para registrar os pedidos dos clientes.

O Amazon EC2 Auto Scaling permite que você adicione ou remova automaticamente instâncias do Amazon EC2 em resposta à alteração da demanda do aplicativo. Ao fazer auto scaling de suas instâncias, aumentando ou reduzindo conforme a necessidade, você consegue manter uma sensação maior de disponibilidade de aplicativos.

No Amazon EC2 Auto Scaling, há duas abordagens disponíveis: scaling dinâmico e scaling preditivo.

•	O scaling dinâmico responde às alterações na demanda. 
•	O scaling preditivo programa automaticamente o número correto de instância do Amazon EC2 com base na demanda prevista.

### Elastic Load Balancing

O Elastic Load Balancing é o serviço AWS que distribui automaticamente o tráfego de entrada de aplicativos entre vários recursos, como instâncias do Amazon EC2.

Um balanceador de carga atua como um ponto único de contato para todo o tráfego da web de entrada no seu grupo do Auto Scaling. Isso significa que, à medida que você adiciona ou remove instâncias do Amazon EC2 em resposta à quantidade de tráfego de entrada, essas solicitações são direcionadas para o balanceador de carga primeiro. Em seguida, as solicitações se espalham por vários recursos que lidarão com elas. Por exemplo, se você tiver várias instâncias do Amazon EC2, o Elastic Load Balancing distribuirá a carga de trabalho entre elas para que nenhuma instância tenha que carregar a maior parte.

Embora o Elastic Load Balancing e o Amazon EC2 Auto Scaling sejam serviços separados, eles trabalham juntos para que os aplicativos executados no Amazon EC2 possam fornecer alto desempenho e disponibilidade.

### Aplicativos monolíticos e microsserviços
Os aplicativos são formados por vários componentes. Os componentes se comunicam entre si para transmitir dados, atender solicitações e manter o aplicativo em execução.

Suponha que você tenha um aplicativo com componentes com acoplamento forte. Esses componentes podem ser bancos de dados, servidores, interface do usuário, lógica de negócios e assim por diante. Esse tipo de arquitetura pode ser considerado um aplicativo monolítico.
Nessa abordagem à arquitetura do aplicativo, se um único componente falhar, outros componentes falharão e possivelmente todo o aplicativo.

### Amazon Simple Notification Service (Amazon SNS)

O Amazon Simple Notification Service (Amazon SNS) é um serviço de publicação/assinatura. Usando tópicos do Amazon SNS, um editor publica mensagens para assinantes. Isso se parece com a cafeteria: o operador de caixa entrega os pedidos ao barista que, por sua vez, prepara as bebidas.

### Amazon Simple Queue Service (Amazon SQS)

O Amazon Simple Queue Service (Amazon SQS) é um serviço de enfileiramento de mensagens. 

Use o Amazon SQS para enviar, armazenar e receber mensagens entre componentes de software, sem perder mensagens ou precisar que outros serviços estejam disponíveis. No Amazon SQS, um aplicativo envia mensagens para uma fila. Um usuário ou serviço recupera uma mensagem da fila, processa-a e a exclui da fila.

### Computação sem servidor

No início deste módulo, você conheceu o Amazon EC2, um serviço que permite executar servidores virtuais na nuvem. Se você quiser executar aplicativos no Amazon EC2, faça o seguinte:

•	Provisione as instâncias (servidores virtuais).
•	Faça upload do código.
•	Continue gerenciando as instâncias enquanto o aplicativo está em execução.

O termo “sem servidor” significa que o código é executado em servidores, sem que você precise provisionar ou gerenciar esses servidores. Com a computação sem servidor, você pode se concentrar na inovação de novos produtos e recursos em vez de manter servidores.
Outro benefício da computação sem servidor é a flexibilidade de dimensionar aplicativos sem servidor automaticamente. A computação sem servidor pode ajustar a capacidade de aplicativos modificando as unidades de consumo, como taxa de transferência e memória.

Um serviço AWS para computação sem servidor é o AWS Lambda.

### AWS Lambda

O AWS Lambda é um serviço que permite a execução de códigos sem a necessidade de provisionar ou gerenciar servidores.
Ao usar o AWS Lambda, você paga apenas pelo tempo de computação que consumir. As cobranças se aplicam ao tempo em que o código fica em execução. Você pode executar códigos para praticamente qualquer tipo de aplicativo ou serviço de back-end sem a necessidade de qualquer gerenciamento.

Por exemplo, uma função simples do Lambda é o redimensionamento automático de imagens com o upload feito na nuvem AWS. Nesse caso, a função é acionada ao fazer upload de uma nova imagem.

### Amazon Elastic Container Service (Amazon ECS)

O Amazon Elastic Container Service (Amazon ECS) é um sistema de gerenciamento de contêineres altamente dimensionável e de alto desempenho que permite executar e dimensionar aplicativos em contêineres na AWS.

O Amazon ECS é compatível com contêineres Docker. O Docker é uma plataforma de software que permite criar, testar e implantar aplicativos rapidamente. A AWS é compatível com c Docker Community Edition de código aberto e do Docker Enterprise Edition baseado em assinatura. Com o Amazon ECS, você pode usar chamadas de API para iniciar e interromper aplicativos ativados pelo Docker.

### Amazon Elastic Kubernetes Service (Amazon EKS)

O Amazon Elastic Kubernetes Service (Amazon EKS) é um serviço totalmente gerenciado que você pode usar para executar o Kubernetes na AWS.
O Kubernetes é um software de código aberto que permite implantar e gerenciar aplicativos em contêineres em grande escala. Uma grande comunidade de voluntários mantém o Kubernetes, e a AWS trabalha ativamente em conjunto com essa comunidade Kubernetes. Conforme novos recursos e funcionalidades são lançados para aplicativos Kubernetes, você pode facilmente aplicar essas atualizações aos aplicativos gerenciados pelo Amazon EKS.

### AWS Fargate

O AWS Fargate é um mecanismo de computação sem servidor para contêineres. Ele funciona com o Amazon ECS e o Amazon EKS.
Com o AWS Fargate, você não precisa provisionar ou gerenciar servidores. O AWS Fargate gerencia sua infraestrutura de servidor para você. Você pode se concentrar em inovar e desenvolver seus aplicativos, pagando apenas pelos recursos necessários para executar os contêineres.