# Sprint 1 - Processos Ágeis e Fundamentos de Teste

## Descrição
Repositório criado para a realização  avaliação da primeira sprint do Programa de Bolsas da Compass UOL - AWS for Software Quality & Test Automation.

Nessa primeira sprint por objetivo desenvolver as seguintes habilidades:
 - Vivenciar ambiente de metodologia ágil.
 - Aprofundar conhecimentos em .NET Core;
 - HTTP, segurança de redes, segurança web;
 - Aprender sobre Git e GitHub;
 - C#, .NET, SOLID com C#;
 - .NET e MongoDB;
 - servidor SQL
 - API Rest com .Net Core;

 ## Desenvolvimento
  - Realizar uso do Git e Gitlab para versionamento de código e também objetos de aprendizado;
  - Criar um README para a Sprint atual seguindo as dicas de conteúdos que serão passadas;
  - Realizar o versionamento dos exercícios propostos durante a Sprint;
  - Organização/resumo de todos conteúdos em arquivos separados por dias para uma apresentação ao final.

## Entrega
Ao final da Sprint será reservado um tempo de no máximo 3 minutos para cada apresentação. Na apresentação o projeto do Gitlab deverá ser apresentado e um resumo do que foi versionado durante a Sprint.