# Dia 2 :: Ágil
## - Planejamento da Sprint 1;
## - Entregáveis;
## - Orientações Gerais.

Scrum é um método ágil de gerenciamento de projetos de software. Ele se concentra em ciclos curtos de desenvolvimento, chamados de sprints, nos quais uma equipe de desenvolvimento trabalha em pequenos incrementos do produto, com feedback constante dos clientes ou usuários finais. Scrum é frequentemente usado em projetos de software, mas também pode ser usado em outras áreas de negócios.

O Scrum é baseado em três funções principais: o Product Owner, o Scrum Master e a equipe de desenvolvimento. O Product Owner é responsável por definir os requisitos do produto e priorizar o backlog do produto. O Scrum Master é responsável por garantir que uma equipe de desenvolvimento siga as práticas e os valores do Scrum. A equipe de desenvolvimento é responsável por criar o produto durante o sprint.

O Scrum utiliza preservados como o Product Backlog, Sprint Backlog e Incremento para garantir que a equipe esteja trabalhando em direção a um objetivo comum. O Product Backlog contém uma lista de todos os requisitos do produto, classificados por prioridade. O Sprint Backlog contém uma lista das tarefas que uma equipe deve realizar durante o sprint. O Incremento é o resultado do sprint, um produto funcionando que pode ser lançado ou entregue.

O Scrum utiliza reuniões de acompanhamento, reuniões de planejamento de sprint, revisões de sprint e retrospectivas de sprint para garantir que a equipe esteja trabalhando de forma colaborativa e eficiente. As reuniões temporárias são breves reuniões para que a equipe de desenvolvimento se mantenha sincronizada. As reuniões de planejamento de sprint permitem que a equipe defina as metas do sprint e identifique as tarefas a serem concluídas. As revisões de sprint permitem que a equipe mostre o produto finalizado aos stakeholders e obtenha feedback. As retrospectivas de sprint permitem que a equipa reflita sobre o sprint anterior e que evite maneiras de melhorar no próximo sprint.