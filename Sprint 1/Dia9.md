### Fundamentos da Aquisição da Nuvem

Diferentes aspetos levam e suportam a adoção da nuvem. Embora a aquisição seja central na sua jornada para a nuvem, existem outros aspetos importantes que se sobrepõem e se interligam entre si e que contribuem para esta experiência. Aqui estão algumas dicas importantes quando se trata de fundamentos da aquisição da nuvem.

### Bases da Aquisição

As estratégias para a compra de tecnologia legada não funcionarão para a nuvem. Isto inclui políticas e estruturas na sua organização. Para obter os benefícios da nuvem, tem de mudar o seu pensamento. Aqui estão algumas dicas importantes quando se trata de bases de aquisição.

## Aspetos principais da aquisição

Seis áreas principais afetam a forma como a nuvem é comprada: preços, segurança, residência de dados, sustentabilidade, administração e termos e condições. Se não entender totalmente estas áreas, isto pode impedir que obtenha todos os benefícios da nuvem.

Aqui estão algumas dicas quando se trata dos aspetos principais da aquisição: 
- O preço da nuvem é variável. Aumenta e diminui ao longo do tempo com base no preço do fornecedor – não é específico para os clientes.
- Deverá avaliar a segurança e resiliência da infraestrutura física da AWS utilizando certificações estabelecidas por terceiros independentes em vez de exigir que faça auditorias e avalie estes elementos. A utilização destes recursos existentes evita processos demasiado onerosos e duplicações.
- É importante compreender a soberania de dados e residência de dados desde cedo para garantir que pode maximizar os benefícios da oferta da AWS Cloud.
- Qualquer análise sobre o impacto climático de um centro de dados deve considerar a utilização de recursos e a eficiência energética, além da mistura de energia. Os Fornecedores de Serviços de Cloud (CSPs) de grandes dimensões utilizam uma combinação de fontes energéticas que é 28% menos intensa em carbono do que a média global.
- Deve considerar a administração da nuvem para operacionalizar com sucesso o seu ambiente, o que deve ser considerado com bastante antecedência em relação a uma aquisição. Visto que mantém o controlo sobre os seus dados, como parte de qualquer estratégia de aquisição, deve avaliar os recursos dos Fornecedores de Serviços de Cloud (CSPs) para atender às suas necessidades de administração da nuvem.
- Os termos e condições da nuvem são desenvolvidos para refletir como um modelo de serviços em nuvem funciona (ativos físicos não estão a ser adquiridos). Os Fornecedores de Serviços de Cloud (CSPs) operam num dimensionamento maciço de «um para muitos» para oferecer serviços padronizados e não podem personalizar os serviços para as necessidades individuais do cliente. Portanto, é fundamental que envolva os Fornecedores de Serviços de Cloud (CSPs) atempadamente para integrar e utilizar os seus termos na medida do possível.

### Trabalhar com Parceiros

A Rede de Parceiros da AWS pode fornecer uma vasta gama de valor e suporte. Considere a APN e o que pode oferecer como parte de qualquer possível aquisição.

Aqui estão algumas dicas importantes quando se trata de trabalhar com a APN:

- Visite a página web APN para ver como os parceiros da AWS podem ajudá-lo a criar soluções de sucesso baseadas na AWS.
- É importante envolver atempadamente o seu representante de vendas da AWS e frequentemente para garantir que é capaz de avaliar e selecionar Parceiros que o possam ajudar a satisfazer os seus requisitos.
- A APN é o programa global de parceiros para empresas de tecnologia e consultoria que utilizam a AWS para criar soluções e serviços para clientes.
- Os Parceiros da AWS estão numa posição única para o ajudar a acelerar a sua viagem para a nuvem e a tirar o máximo partido de tudo o que a AWS tem para oferecer. Os parceiros podem ajudá-lo a identificar soluções e a construir, implementar ou migrar para a AWS.

### AWS Well-Architected Framework 

O AWS Well-Architected Framework é um conjunto de melhores práticas e orientações fornecido pela Amazon Web Services (AWS) para auxiliar na criação de arquiteturas seguras, de alta qualidade, eficientes e confiáveis no ambiente de nuvem. Ele é projetado para ajudar as organizações a tomar decisões informadas ao projetar e implantar aplicativos e workloads na AWS.

Os recursos do AWS Well-Architected Framework incluem:

1. Princípios gerais de desenvolvimento na nuvem: Esses princípios orientam as organizações a adotarem abordagens ágeis, automatizadas e baseadas em serviços para desenvolver aplicativos na nuvem. Isso inclui o uso de microsserviços, a adoção de práticas de DevOps e a implementação de processos contínuos de integração e entrega.

2. Pilares do AWS Well-Architected Framework: O framework é baseado em cinco pilares fundamentais que devem ser considerados ao projetar arquiteturas na nuvem:

   a. Segurança: Garante a proteção adequada dos dados, sistemas e recursos na nuvem, incluindo a implementação de controles de segurança, a gestão de identidades e acessos, e a resiliência contra ameaças.

   b. Confiabilidade: Foca na capacidade de um sistema em operar corretamente e se recuperar de falhas de forma rápida e eficiente. Isso envolve o uso de arquiteturas resilientes, a redundância dos recursos e a implementação de estratégias de recuperação de desastres.

   c. Eficiência de desempenho: Busca otimizar o uso de recursos, garantir a capacidade adequada para as demandas do sistema e minimizar custos. Isso inclui o dimensionamento apropriado, o monitoramento do desempenho e a otimização dos recursos.

   d. Excelência operacional: Concentra-se na automação, na gestão e na melhoria contínua dos processos operacionais. Isso envolve o uso de ferramentas de automação, o monitoramento proativo, a documentação adequada e a implementação de práticas de gerenciamento de alterações.

   e. Otimização de custos: Visa maximizar o valor dos investimentos na nuvem, evitando gastos desnecessários e buscando otimizar o uso dos recursos. Isso envolve o monitoramento e a análise dos custos, a identificação de oportunidades de otimização e a adoção de práticas de governança financeira.

3. Usos comuns do AWS Well-Architected Framework: O framework pode ser utilizado em diferentes estágios do ciclo de vida de uma aplicação, desde a fase de planejamento e design até a operação contínua. Ele é útil para avaliar a arquitetura existente, identificar áreas de melhoria, tomar decisões informadas sobre a adoção de serviços e recursos da AWS, e garantir a conformidade com as melhores práticas de arquitetura na nuvem.

Em resumo, o AWS Well-Architected Framework fornece orientações valiosas para projetar e construir arquiteturas eficientes e confiáveis na nuvem, com foco na segurança, confiabilidade, eficiência de desempenho, excelência operacional