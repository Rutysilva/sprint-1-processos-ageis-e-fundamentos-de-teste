# Dia 4 :: MasterClass
## Fundamentos do teste de software
## Seção 2

O teste de software tem uma história que remonta aos primórdios da computação. Desde os primeiros sistemas computacionais, o teste tem sido realizado para verificar a funcionalidade e confiabilidade dos programas. Com o tempo, o teste de software evoluiu como uma disciplina independente, com metodologias e práticas específicas para garantir a qualidade do software.

A importância do teste de software é inegável. Ele desempenha um papel fundamental na identificação e correção de bugs e defeitos antes que o software seja lançado. Bugs não detectados podem ter consequências negativas, como falhas no sistema, perda de dados, danos fiscais e financeiros. O teste adequado e abrangente ajuda a reduzir esses riscos e melhorar a qualidade do software.

Existem sete fundamentos que guiam a abordagem de teste de software. Esses fundamentos incluem o fato de que o teste mostra a presença de defeitos, mas nunca a sua ausência. Isso significa que o teste pode revelar problemas, mas não pode garantir que não haja mais defeitos ocultos. Além disso, é importante entender que o teste exaustivo não é possível. Devido à complexidade dos sistemas de software, é impraticável testar todas as combinações e possíveis. Portanto, os testadores usam técnicas de seleção de teste estratégico.

O teste antecipado é outro princípio importante. Incorporar o teste desde as fases iniciais do ciclo de vida do desenvolvimento de software permite identificar e corrigir defeitos mais cedo, evitando problemas mais graves no futuro. O agrupamento de defeitos é um fenômeno em que vários defeitos são causados ​​por uma única falha ou causa raiz. Identificar e corrigir a causa raiz pode eliminar vários defeitos de uma só vez, economizando tempo e esforço.

No entanto, é essencial reconhecer que o teste de software não pode ser exaustivo. Dada a complexidade dos sistemas, é impossível testar todas as possibilidades. O teste depende do contexto e das prioridades do projeto, e os testadores precisam tomar decisões estratégicas sobre quais áreas e cenários devem ser testados.

Há também a ilusão da ausência de erros, onde a ausência de defeitos encontrados durante o teste não garante que o software esteja livre de erros. Por fim, é importante distinguir entre teste e QA (Garantia de Qualidade). O teste de software é uma parte do processo de QA, que abrange atividades mais amplas, incluindo planejamento, revisão de requisitos, auditoria e melhoria contínua.

Existem vários tipos de testes baseados na norma IEC/ISO 25010, que abrangem áreas como melhorias funcionais, usabilidade, compatibilidade, confiança, eficiência no desempenho, manutenibilidade, portabilidade e segurança. Esses tipos de teste visam avaliar diferentes aspectos da qualidade do software, garantindo que ele atenda aos requisitos e às expectativas dos usuários.

Por fim, o teste de software pode ser realizado manualmente ou por meio de automação. Os testes manuais a execução de casos de teste por testadores humanos, enquanto os testes concluídos são realizados por meio de scripts

