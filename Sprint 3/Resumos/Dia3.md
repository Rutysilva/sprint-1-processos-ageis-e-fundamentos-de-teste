### Por que escrever histórias de usuário? 

Uma pergunta muito comum que às vezes escutamos quando falamos em user story é: Por que devemos escrevê-las? Podemos dizer que, para o desenvolvimento de um produto, precisamos orientar nosso time sobre qual a nossa necessidade enquanto negócio, ou seja, o que deve ser construído. Mas dizer somente o que precisamos construir, muitas vezes não se mostra suficiente para o desenvolvimento da melhor solução, pois pode gerar muitas formas de interpretação pelo time impactando na entrega final ao usuário. 

Precisamos então fornecer mais informações para contextualizar nossos desenvolvedores, para isso é preciso dizer a eles para quem eles irão desenvolver alguma funcionalidade: Quem é o meu usuário? Como ele é? Quais as suas dores e necessidades? Além disso, é preciso trazer também o por quê meu usuário deseja aquilo: o que ele espera alcançar com aquela solução? Qual sua expectativa e resultado esperado? 

De uma forma geral, podemos dizer que a escrita das histórias de usuário funcionam como uma narrativa sobre o quê, para quem e porque entregar aquela funcionalidade. É um direcionador ao desenvolvimento!

### Existe um padrão de escrita na história de usuário?

Quando falamos em escrita de histórias de usuário, nos deparamos com alguns modelos mais utilizados (templates) pelos profissionais de produto. Isso nos conduzem a uma melhor representação da necessidade do negócio em uma escrita mais direcionada das features. Aliados a esses modelos, existem também algumas técnicas utilizadas, como o 3W – who (quem)/ what (o quê)/ why (por que) –; e a técnica do 3C – card (cartão)/ conversation (conversa)/ confirmation (confirmação). 

### Template para escrita de User Story 

Um template muito utilizado é o do ‘Como/eu quero/para’, representado na imagem abaixo: 
<div>
  <img src="
https://www.cms.dtidigital.com.br/wp-content/uploads/2021/08/HistoriaDeUsario1.png" width="800" title="Imagem">
</div>

Veja que para escrever uma história de usuário é bem simples, basta que sigamos o modelo apresentado, trazendo as informações: 

- <b>Como -</b> autor da ação (persona); 
- <b>eu quero -</b> funcionalidade desejada; 
- <b>para -</b> valor agregado à funcionalidade desejada.

<b>Técnica 3W</b>

Na utilização dessa técnica devemos nos atentar a quem é o nosso sujeito ou usuário, ou seja, como o usuário se encontra (persona), o que ele deseja realizar e para quê ou porquê ele deseja executar aquela atividade ou ação. Seguindo o modelo apresentado acima temos: 

- Como (quem?) – aqui eu devo referenciar para quem eu estou construindo aquela funcionalidade. Quem é a minha persona atingida por aquela história? 
- eu quero (o quê?) – aqui eu devo colocar qual a funcionalidade esperada. O que minha persona deseja realizar? 
- para (por quê?) – devo descrever qual a motivação para a construção daquela história. Qual o valor esperado pela pessoa usuária? 

<b>Técnica 3C</b> 

Esta técnica nos trás para uma preocupação com o conteúdo dos questionamentos da técnica do 3W. Os 3C representam card (cartão)/ conversation (conversa)/ confirmation (confirmação), onde: 

- Cartão – uma User Story não deve ser muito extensa, tornando possível escrevê-la em um único cartão; 
- Conversa – seu conteúdo deve gerar discussões de alinhamento entre o time sobre o que se deseja atender; 
- Confirmação – é importante que a história de usuário possua critérios de aceitação para que seja possível a validação de êxito na construção e entrega. 

Um exemplo de uma user story, seguindo o template apresentado e as técnicas descritas acima ficaria assim:

<div>
  <img src="https://www.cms.dtidigital.com.br/wp-content/uploads/2021/08/HistoriaDeUsuario2.png" width="800" title="Imagem">
</div>

### Critérios de Aceitação (Acceptance Criteria) 

Na escrita das histórias também devemos possuir os critérios de aceitação. Eles são como check lists que devem ser repassados para garantir que aquela história esteja entregando o que foi pedido e planejado em sua definição, em caráter de funcionalidade para entrega de valor ao usuário. Eles funcionam como uma forma de detalhamento maior sobre o que deve ser entregue naquela história, auxiliando também os desenvolvedores na definição e planejamento dos testes. 

Não existe uma forma correta de escrita desses critérios de aceitação, eles podem ser em forma de tópicos ou mesmo através da descrição de possíveis cenários de uso do usuário. 

### Defintion of Ready (DoR)
Defintion of Ready (DoR) é um acordo de trabalho entre o time de Desenvolvimento e o Product Owner, aplicado a todas as Histórias de Usuário, com a intenção de que os itens do Backlog não cheguem para a reunião de planejamento com granularidade ruim, pouco ou nenhum detalhamento.

### Definition of Done (DoD)
Definition of Done (DoD) é um acordo genérico, definido pelos membros do time Scrum (Desenvolvedores e Product Owner), aplicável a todas as Histórias de Usuário, com o intuito de que todos os membros do time tenham um entendimento compartilhado do que significa “Done” para garantir a transparência. Ou seja, uma lista de verificação de atividades necessárias para que um incremento de software seja considerado como completo.